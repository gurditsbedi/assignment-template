
CC=xelatex

assignment: assignment.tex
	$(CC) assignment.tex

clean:
	rm -rf *.aux *.log *.class *.toc
